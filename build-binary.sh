#/bin/bash

set -eu

INSTALL_DIR="/opt/fluentbit"
WORK_DIR="${INSTALL_DIR}"
BUILD_DIR="${WORK_DIR}/build"
EXEC_FILE="${INSTALL_DIR}/bin/fluent-bit"
LIB_DIR="${INSTALL_DIR}/lib/"

echo "=========== Start build fluentbit ==========="

# Check the build dir whether exist.
if [[ ! -d ${BUILD_DIR} ]]; then
    echo "The dir ${BUILD_DIR} does not exist."
    exit 1
fi

# Build project
cd ${BUILD_DIR}
cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR} ..
make && make install

# Check the binary file whether exist.
if [[ ! -f ${EXEC_FILE} ]]; then
    echo "The binary file ${EXEC_FILE} does not exist."
    exit 1
fi
so_list=$(ldd ${EXEC_FILE} | awk '{if (match($3, "/")){print $3}}')

# Check the lib dir whether exist.
if [[ ! -d ${LIB_DIR} ]]; then
    echo "The dir ${LIB_DIR} does not exist."
    exit 1
fi
cp -L -n $so_list ${LIB_DIR}

echo "=========== Build fluentbit success ==========="
