#!/usr/bin/env bash

set -xe

FLUENTBIT_EXEC="/opt/fluent-bit/bin/fluent-bit"
EXEC_CMD="-c"
CONF_PATH="/opt/fluent-bit/etc/fluent-bit/fluent-bit.conf"
NORMAL_HADOOP="hadoop"


main() {

    exec gosu ${NORMAL_HADOOP} ${FLUENTBIT_EXEC} ${EXEC_CMD} ${CONF_PATH}
}

main "$@"